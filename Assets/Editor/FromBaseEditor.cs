﻿using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(FromBase))]
public class FromBaseEditor : Editor {
    private FromBase fromBase;
    private void Awake() {
        fromBase = (FromBase)target;
    }
    public override void OnInspectorGUI() {
        GUILayout.BeginHorizontal();
        if (GUILayout.Button("NewFrom")) {
            fromBase.CreateFrome();
        }
        if (GUILayout.Button("RemoveFrom")) {
            fromBase.RemoveFrom();
        }
        if (GUILayout.Button("<--")) {
            fromBase.PrevFrom();
        }
        if (GUILayout.Button("-->")) {
            fromBase.NextFrom();
        }
        GUILayout.EndHorizontal();
        base.OnInspectorGUI();
    }
}
