﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "New From Database", menuName = "Databases/Froms")]

public class FromBase : ScriptableObject {
    [SerializeField, HideInInspector] private List<From> froms;
    [SerializeField] private From currentFrom;
    private int currentIndex;

    public void CreateFrome() {
        if (froms == null) froms =new List<From>();

        From from = new From();
        froms.Add(from);
        currentFrom = from;

        currentIndex = froms.Count - 1;
    }
    public void RemoveFrom() {
        if (froms == null) return;

        if (currentFrom == null) return;

        froms.Remove(currentFrom);

        if (froms.Count > 0) currentFrom = froms[0];
        else CreateFrome();
        currentIndex = 0;
    }
    public void NextFrom() {
        if (currentIndex + 1 < froms.Count) {
            currentIndex++;
            currentFrom = froms[currentIndex];
        }
    }
    public void PrevFrom() {
        if (currentIndex > 0) {
            currentIndex--;
            currentFrom = froms[currentIndex];
        }
    }
    public From GetItemOfID(int id) {
        return froms.Find(t => t.ID == id);
    }
}
[System.Serializable]
public class From {
    [SerializeField] private int id;
    public int ID {
        get {return id;}
    }
    [SerializeField] private string fromName;
    public string FromName {
        get {return fromName;}
    }
    [SerializeField] private string vvpName;
    public string VvpName {
        get {return vvpName;}
    }
    [SerializeField] private string popName;
    public string PopName {
        get {return popName;}
    }
    [SerializeField] private string squreName;
    public string SquareName {
        get{return squreName;}
    }
}