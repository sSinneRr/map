﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FromComponent : MonoBehaviour {
    [SerializeField] private FromType type;
    [SerializeField] private Text tName;
    [SerializeField] private Text tVvp;
    [SerializeField] private Text tPop;
    [SerializeField] private Text tSquare;
    private From from;
    public From From {get{return from;}}
    void Start() {
        //from = GameManager.Instance.fromDataBase.GetItemOfID((int)type);       
    }
}
public enum FromType {
    FromRussia = 0, 
    FromFrance = 1, 
    FromItaly = 2
}
