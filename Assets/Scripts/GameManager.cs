﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class GameManager : MonoBehaviour {
   #region Singleton
  public static GameManager Instance {get; private set; }
    #endregion
 [SerializeField] private GameObject panel;
 [SerializeField] private Animator aninator;
 private int uiOpener = 1;
 public FromBase fromDataBase; 
 public Dictionary <GameManager, FromComponent> itemsContainer;

 private void Awake() {
    itemsContainer = new Dictionary<GameManager, FromComponent>();
 }

 public void OnClicPanelFrom() {
    
    if (uiOpener > 0) {
       panel.gameObject.SetActive(true);
       uiOpener = 0;
       }
    else {
       panel.gameObject.SetActive(false);   
       uiOpener = 1;
    }  
 }
}

