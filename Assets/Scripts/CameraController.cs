﻿using UnityEngine;

public class CameraController : MonoBehaviour {
    private int screenWidth;
    private int screenHeight;
    public float speed;
    public bool useCameraMovement;
    void Start() {
        screenWidth = Screen.width;
        screenHeight = Screen.height;       
    }
    void Update() {
       if(Input.touchCount > 0) Debug.Log("Click");
       Vector3 camPos = transform.position;

       if(Input.mousePosition.x <= 20) {
           camPos.x -= Time.deltaTime * speed;
           camPos.z += Time.deltaTime * speed;
       }
       else if (Input.mousePosition.x >= screenWidth - 20) {
           camPos.x += Time.deltaTime * speed;
           camPos.z -= Time.deltaTime * speed;
       }
       else if (Input.mousePosition.y <= 20) {
           camPos.x -= Time.deltaTime * speed;
           camPos.z -= Time.deltaTime * speed;
       }
       else if (Input.mousePosition.y >= screenHeight - 20) {
           camPos.x += Time.deltaTime * speed;
           camPos.z += Time.deltaTime * speed;
       }
       transform.position = camPos;
   }
}
